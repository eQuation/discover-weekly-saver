#! usr/bin/env python3

import sys
import base64
import requests
import json
import urllib
import datetime

HOST, PORT = '', 8888

AUTHORIZATION_URL = 'https://accounts.spotify.com'
AUTHORIZATION_ENDPOINT = AUTHORIZATION_URL + '/authorize'

ACCESS_TOKEN_ENDPOINT = 'https://accounts.spotify.com/api/token'

PROFILE_ENDPOINT = 'https://api.spotify.com/v1/me'

CALLBACK_URL = 'http://localhost:8888/callback'
SCOPES = 'playlist-read-private playlist-modify-private playlist-modify-public'

REFRESH_TOKEN_FILE_NAME = 'refreshToken.txt'

urls = (
    '/authorize',
    'auth',
    '/callback',
    'callback'
)


# app = web.application(urls, globals())

class Auth:
    @staticmethod
    def get():
        parameters = {
            'response_type': 'code',
            'client_id': CLIENT_ID,
            'redirect_uri': CALLBACK_URL,
            'scope': SCOPES
        }

        url = '{}?{}'.format(
            AUTHORIZATION_ENDPOINT,
            urllib.parse.urlencode(parameters))

        raise web.seeother(url)


class Callback:
    @staticmethod
    def get():
        bearer_headers = Callback.get_bearer_headers()

        discover_weekly_playlist_id = Callback.get_discover_weekly_playlist_id(
            bearer_headers)

        playlist_owner_id = 'spotify'
        track_list = Callback.get_track_list(
            discover_weekly_playlist_id,
            playlist_owner_id,
            bearer_headers)

        user_id = Callback.get_user_id(bearer_headers)

        new_playlist_id = Callback.create_new_playlist(user_id, bearer_headers)

        response_json = Callback.add_tracks_to_playlist(
            track_list,
            new_playlist_id,
            user_id,
            bearer_headers)

        return response_json

    @staticmethod
    def get_bearer_headers():
        refresh_token = None
        try:
            refresh_token = Callback.get_refresh_token()
        except FileNotFoundError:
            pass
        if refresh_token is None or len(refresh_token) < 1:
            access_token = Callback.get_access_token_from_code()
        else:
            access_token = Callback.get_access_token_from_refresh_token()

        return {
            'Authorization': 'Bearer {}'.format(access_token),
            'Content-Type': 'application/json'
        }

    @staticmethod
    def get_basic_headers():
        byte_string_to_encode = ('{}:{}'.format(CLIENT_ID, CLIENT_SECRET)) \
            .encode('utf-8')
        auth_string = base64.b64encode(byte_string_to_encode).decode('utf-8')

        return {'Authorization': 'Basic {}'.format(auth_string)}

    @staticmethod
    def get_refresh_token():
        input_file = open(REFRESH_TOKEN_FILE_NAME, 'r')
        refresh_token = input_file.read()
        input_file.close()
        return refresh_token

    @staticmethod
    def update_refresh_token(refresh_token):
        refresh_token_file = open(REFRESH_TOKEN_FILE_NAME, 'w')
        refresh_token_file.write(refresh_token)
        refresh_token_file.close()

    @staticmethod
    def get_access_token_from_code():
        code = web.input().code
        if code is None or len(code) < 1:
            print('Unable to extract access code')
            sys.exit(1)

        data = {
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': CALLBACK_URL
        }

        return Callback.post_for_access_token(data)

    @staticmethod
    def get_access_token_from_refresh_token():
        refresh_token = Callback.get_refresh_token()

        data = {
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
            'redirect_uri': CALLBACK_URL
        }

        return Callback.post_for_access_token(data)

    @staticmethod
    def post_for_access_token(data):
        response = requests.post(
            ACCESS_TOKEN_ENDPOINT,
            headers=Callback.get_basic_headers(),
            data=data)

        if not response.ok:
            print('error getting access token')
            exit(1)

        response_json = json.loads(response.text)

        access_token = response_json['access_token']
        if 'refresh_token' in response_json:
            Callback.update_refresh_token(response_json['refresh_token'])

        return access_token

    @staticmethod
    def get_discover_weekly_playlist_id(headers):
        request_url = 'https://api.spotify.com/v1/me/playlists'

        while True:
            response = requests.get(request_url, headers=headers)

            if not response.ok:
                print('error finding playlist')
                exit(1)

            response_json = json.loads(response.text)

            for playlist in response_json['items']:
                if 'Discover Weekly' == playlist['name']:
                    return playlist['id']
            request_url = response_json['next']

            if request_url is None:
                print('error finding playlist')
                exit(1)

    @staticmethod
    def get_track_list(
            playlist_id,
            playlist_owner_id,
            headers):

        response = requests.get(
            'https://api.spotify.com/v1/users/{}/playlists/{}/tracks'.format(
                playlist_owner_id, playlist_id),
            headers=headers)

        if not response.ok:
            print('error getting track list')
            exit(1)

        response_json = json.loads(response.text)

        track_uri_list = []
        for track in response_json['items']:
            track_uri_list.append(track['track']['uri'])
        return track_uri_list

    @staticmethod
    def get_user_id(headers):
        response = requests.get('https://api.spotify.com/v1/me',
                                headers=headers)

        if not response.ok:
            print('error getting user id')
            exit(1)

        return json.loads(response.text)['id']

    @staticmethod
    def create_new_playlist(user_id, headers):
        playlist_name = 'Discover Weekly {}'.format(datetime.date.today())

        data = {
            'name': playlist_name,
            'public': 'false'
        }

        response = requests.post(
            'https://api.spotify.com/v1/users/{}/playlists'.format(user_id),
            headers=headers,
            data=json.dumps(data))

        if not response.ok:
            print('error creating new playlist')
            exit(1)

        return json.loads(response.text)['id']

    @staticmethod
    def add_tracks_to_playlist(
            track_list,
            playlist_id,
            owner_id,
            headers):
        data = {
            'uris': track_list
        }

        response = requests.post(
            'https://api.spotify.com/v1/users/{}/playlists/{}/tracks'.format(
                owner_id, playlist_id),
            headers=headers,
            data=json.dumps(data))

        if not response.ok:
            print('error adding songs to new playlist')
            exit(1)

        response_json = json.loads(response.text)
        return response_json

# if __name__ == "__main__":
# Web version
# app.run()
